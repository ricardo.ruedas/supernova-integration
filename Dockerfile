FROM registry.gitlab.com/wallbox/embedded/embedded-testing/embedded-testing:1.44.0

RUN test -f /etc/apt/.org_sources.list \
 && cp /etc/apt/.org_sources.list /etc/apt/sources.list

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt upgrade -y

RUN apt install -y \
  sudo \
  gdb \
  libqt5quick5 \
  qtdeclarative5-dev \
  qml-module-qtquick-controls2 \
  qml-module-qtquick-controls \
  qml-module-qtquick-dialogs \
  qml-module-qtquick-layouts \
  qml-module-qtquick-privatewidgets \
  qml-module-qtquick-templates2 \
  qml-module-qtquick-virtualkeyboard \
  qml-module-qtquick-window2 \
  qml-module-qtquick2 \
  qtquickcontrols2-5-dev \
  qtquickcontrols2-5-doc \
  qtquickcontrols5-doc \
  libboost-program-options1.71.0

RUN apt install -y tcpdump

#ADD micro2wallbox/src/micro2wallbox /usr/bin/
#ADD libwallbox/src/libwallbox.so /lib/x86_64-linux-gnu/
#ADD libwallbox_ipc/src/libwallbox_ipc.so /lib/x86_64-linux-gnu/
ADD libwallbox_db/src/libwallbox_db.so /lib/x86_64-linux-gnu/
#ADD libwallbox_hw/src/libwallbox_hw.so /lib/x86_64-linux-gnu/
ADD libwallbox_sh/libwallbox_utils/src/libwallbox_utils.so /lib/x86_64-linux-gnu/
#ADD libwallbox_sh/libwallbox_redis/src/libwallbox_redis.so /lib/x86_64-linux-gnu/
ADD libwallbox_db/db_updater/db_updater /usr/bin/

RUN nohup bash -c "/usr/sbin/mysqld &" \
 && nohup bash -c "/usr/bin/redis-server &" \
 && sleep 5 \
 && db_updater \
 && killall -v -w mysqld \
 && sleep 1

RUN mkdir -p /var/lib/mysql && chmod o=u -R /var/lib/mysql
RUN mkdir -p /var/log/mysql && chmod o=u -R /var/log/mysql
RUN mkdir -p /var/run/mysqld && chmod o=u -R /var/run/mysqld
RUN chmod o=u -R /opt

COPY supernova_arch_proto_phase_01_py /opt/supernova_arch_proto_phase_01_py/
RUN cd /opt/supernova_arch_proto_phase_01_py \
 && pip3 install -r requirements.txt

#ADD external/libcml-ocppj/src/libcml-ocpp.so /lib/x86_64-linux-gnu/
ADD _deps/cml-ocpp-build/src/libcml-ocpp.so /lib/x86_64-linux-gnu/
ADD libserialization/libserialization.so /lib/x86_64-linux-gnu/
ADD libwallbox_sh/service_base_lib/libservice_base.so /lib/x86_64-linux-gnu/
ADD messages_definition/libmessages_definition.so /lib/x86_64-linux-gnu/
ADD messaging_lib/messaging_base_lib/libmessaging_base_lib.so /lib/x86_64-linux-gnu/
ADD pubsub/brokers/pubsub_redis/libpubsub_redis.so /lib/x86_64-linux-gnu/
ADD pubsub/pubsub_addon_msg/libpubsub_addon_msg.so /lib/x86_64-linux-gnu/
ADD pubsub/pubsub_addon_rpc_lib/libpubsub_addon_rpc_lib.so /lib/x86_64-linux-gnu/
ADD pubsub/pubsub_factory/libpubsub_factory.so /lib/x86_64-linux-gnu/

ADD cfg/cfg/cfg /usr/bin/
ADD main_control/main_control /usr/bin/
ADD hmi_supernova/hmi_supernova /usr/bin/
ADD ocpp_client/ocpp_client /usr/bin
ADD power_manager/power_manager /usr/bin/
ADD tools/m4_simulator/simulator/m4_simulator /usr/bin/
ADD tools/m4_simulator/client/m4_simulator_client /usr/bin/
#ADD web_token/web_token_app/web_token /usr/bin/
#ADD telemetry/telemetry_srvc/telemetry_srvc /usr/bin/
#ADD avro_schema_v1.1.json /opt/
#ADD telemetry_srvc.config /etc/wallbox/
RUN chmod a+s /usr/sbin/tcpdump

ADD py_sup_venv /opt/

RUN mkdir -p /etc/py_sup \
 && echo \
"[DB]\n" \
"uri=mysql+pymysql://root:fJmExsJgmKV7cq8H@localhost/wallbox?charset=utf8mb4\n" \
  > /etc/py_sup/global_config.ini

RUN mkdir -p /etc/py_sup \
 && echo \
"[CHARGE_SESSION]\n" \
"initializing_state_timeout=60\n" \
"ban_timeout=10\n" \
  > /etc/py_sup/charge_session_config.ini

RUN mkdir -p /home/root/.wallbox && chmod o=u -R /home/root/.wallbox

RUN echo \
"#!/bin/bash\n" \
"export PYTHONPATH=${PYTHONPATH}:/opt/py_sup_venv/lib/python3.8/site-packages/:/usr/lib/python3/dist-packages:/opt/py_sup_venv/lib/python3.8/site-packages\n" \
"export EMBEDDED_API=http://127.0.0.1:8082\n" \
"export LOG_LEVEL=0\n" \
"export LOG_PRINT_LEVEL=0\n" \
"export LOG_SYNC=1\n" \
"cp /home/rruedas/git/embedded-testing/bin/start.bin /home/root/.wallbox/\n" \
"/usr/sbin/tcpdump -i any -s 65535 -w /tmp/net_log.pcap &>/tmp/tcpdump.log &\n" \
"/usr/sbin/mysqld &>/tmp/mysqld.log &\n" \
"/usr/bin/redis-server &>/tmp/redis.log &\n" \
"/usr/bin/m4_simulator &>/tmp/m4_simulator.log &\n" \
"sleep 2\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"delete from users where user_id > 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into users(user_id, name, owner_email, rfid) values(2, 'alice', 'alice@wallbox.com', '0002')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into users(user_id, name, owner_email, rfid) values(3, 'bob', 'bob@wallbox.com', '0003')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update charger_info set software_version = '0.0.999'\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update ocpp_config set enable = 1, url = 'ws://ocpp-service:8180/steve/websocket/CentralSystemService/', charger_id = 'docker' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update telemetry_config set broker_type_charger = 'REDIS' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update telemetry_config set broker_tcp_port_charger = 6379 where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update telemetry_config set avro_schema_path = '/opt/avro_schema_v1.1.json' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update telemetry_config set security_ca_path = '/tmp/aws_credentials/root-CA.crt' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update telemetry_config set security_pub_key_path = '/tmp/aws_credentials/public.key' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update telemetry_config set security_priv_key_path = '/tmp/aws_credentials/private.key' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update telemetry_config set security_cert_path = '/tmp/aws_credentials/cert.pem' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"update myconfig set hostname = 'api.wall-box.com' where id = 1\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into generic_configuration_id values ('WALLBOX_HMI_SUPERNOVA', '4f8fdee9355333d22b3f9ecc97c216ca7ee299e86e8a1eb3118dedcd747ab713')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into generic_configuration_id values ('WALLBOX_POWER_MANAGER', '78fcb017dc4e341702f49c986b7f0a1778f706be76bfe2721d71d6efb6adbd4e')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into generic_configuration_data values ('WALLBOX_HMI_SUPERNOVA', 'Version', 1)\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into generic_configuration_data values ('WALLBOX_HMI_SUPERNOVA', 'Default_Locale', 'es_ES')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into generic_configuration_data values ('WALLBOX_POWER_MANAGER', 'struct_version', '1')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into generic_configuration_data values ('WALLBOX_POWER_MANAGER', 'power_limit_kw', '10')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into recovery_data values ('POWER_MANAGER', 'MaxPowerLimit', 'MaxPowerLimit', 1, '{\\\"limit\\\":10}')\"\n" \
"mysql -uroot -pfJmExsJgmKV7cq8H wallbox -e \"insert into recovery_data values ('ocpp-client', 'connector_availability', 'Availability', 1, '{\\\"connectors\\\":[{\\\"available\\\":\\\"AVAILABLE\\\", \\\"connector_id\\\":\\\"ID1\\\"}, {\\\"available\\\":\\\"AVAILABLE\\\", \\\"connector_id\\\":\\\"ID2\\\"}]}')\"\n" \
"redis-cli monitor &>/tmp/redis-mon.log &\n" \
"/usr/bin/cfg &>/tmp/cfg.log &\n" \
#"/usr/bin/web_token &>/tmp/web_token.log &\n" \
"sleep 1\n" \
"cd /opt/supernova_arch_proto_phase_01_py && python3 charge_session/app/main.py &>/tmp/charge_session.log &\n" \
"sleep 1\n" \
#"/usr/bin/telemetry_srvc &>/tmp/telemetry.log &\n" \
"cd /opt/supernova_arch_proto_phase_01_py && python3 login/app/main.py &>/tmp/login.log &\n" \
"cd /opt/supernova_arch_proto_phase_01_py && python3 mygateway/main.py &>/tmp/mygateway.log &\n" \
"/usr/bin/ocpp_client -p supernova &>/tmp/ocpp_client.log &\n" \
"/usr/bin/power_manager /dev/pts/2 &>/tmp/power_manager.log &\n" \
"/usr/bin/hmi_supernova &>/tmp/hmi_supernova.log &\n" \
"/usr/bin/main_control &>/tmp/main_control.log &\n" \
"/usr/bin/m4_simulator_client &>/tmp/m4_simulator_client.log &\n" \
"exec \"\$@\"\n" \
  > /opt/entrypoint.sh \
  && chmod +x /opt/entrypoint.sh

ENTRYPOINT ["/opt/entrypoint.sh"]

CMD ["/bin/bash"]
