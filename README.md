# Supernova Integration



## Run Steve

```
docker-compose up
```

Note down IP address.

## Build Software

From the build directory of the `wallbox_sw`, run the following commands, using
the previous IP address in the last command.

```
cmake -DPRODUCT=supernova -DCOMPUTE_MODULE=ATLAS -DCOMPILE_MODE=d ..
cmake --build . -j12 -- -k
cp -r ~/git/supernova_arch_proto_phase_01_py .
docker build --rm -t supernova:latest -f ~/git/supernova-integration/Dockerfile .
docker network create supernova-net
docker_env.sh --add-host mysql-service:127.0.0.1 --add-host redis-service:127.0.0.1 --add-host ocpp-service:172.19.0.3 --network supernova-net supernova
```

## Manual messages

### Read RFID event

```
redis-cli publish /wbx/rfid/event '{"header":{"message_id":"EVENT_READ_RFID"},"body":{"SourceDeviceId":"1", "Key":"00000000000003"}}'
```
