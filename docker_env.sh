#!/bin/bash

docker run -it --rm \
  -u $(id -u):$(id -g) \
  -e DISPLAY \
  -v $HOME:$HOME \
  -v /etc/group:/etc/group:ro \
  -v /etc/passwd:/etc/passwd:ro \
  -v /etc/shadow:/etc/shadow:ro \
  -v /etc/sudoers.d:/etc/sudoers.d:ro \
  -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
  -w $(pwd) \
  $@
